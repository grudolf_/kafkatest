import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import FirstScreen from './FirstScreen';

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Hello',
  };

  render() {
    const {navigate} = this.props.navigation;

    return (
      <View style={styles.container}>
        <Text>Home screen</Text>
        <Button title="To Screen 1" onPress={() => this.props.navigation.push('FirstScreen')}/>
      </View>
    );
  }
}

export default createStackNavigator({
  Home: {
    screen: HomeScreen
  },
  FirstScreen: FirstScreen
});

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
