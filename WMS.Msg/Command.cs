﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WMS.Msg
{
    public class Message
    {
        public Message(Dictionary<string, string> headers, object body)
        {
            Body = body;
            Headers = headers;
        }

        /// <summary>
        /// Deserializiraj sporočilo
        /// </summary>
        /// <param name="serializedMessage"></param>
        public Message(string serializedMessage)
        {
            JObject parsedMessage;
            try
            {
                parsedMessage = JObject.Parse(serializedMessage);
            }
            catch (JsonReaderException ex)
            {
                //doesn't look like anything to me
                Console.WriteLine($"Invalid json: {serializedMessage}");
                return;
            }
            var msgHeader = parsedMessage["Headers"];
            Headers = msgHeader.ToObject<Dictionary<string, string>>();
            var msgType = Headers["msg-type"];
            Type type = Type.GetType(msgType);
            if (type == null)
                type = Type.GetType(msgType + ", WMS.Msg");
            if (type != null)
            {
                var msgBody = parsedMessage["Body"];
                Body = msgBody.ToObject(type);
            }
        }

        public Dictionary<string, string> Headers { get; private set; }
        public object Body { get; private set; }
    }

    public class Message<T> : Message
    {
        public Message(Dictionary<string, string> headers, object body) : base(headers, body)
        {
        }

        public Message(string serializedMessage) : base(serializedMessage)
        {
        }

        public new T Body { get; private set; }
    }

    public abstract class Command //: Message
    {
    }

    public abstract class Event //: Message
    {

    }

    public static class Headers
    {
        public const string MessageId = "msg-id";
        public const string Type = "msg-type";
        public const string CorrelationId = "correlation-id";
        public const string DateCreated = "date-created";
    }

}
