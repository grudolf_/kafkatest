﻿namespace WMS.Msg.TransportUnits
{
    public class MoveCommand : Command
    {
        public int Id { get; set; }
        public int? SrcLocation { get; set; }
        public int DstLocation { get; set; }
    }

    public class MovedEvent : Event
    {
        public int Id { get; set; }
        public int? SrcLocation { get; set; }
        public int DstLocation { get; set; }
    }

    public class InvalidUnitEvent : Event
    {
        public int Id { get; set; }
        public int? SrcLocation { get; set; }
        public int DstLocation { get; set; }
    }

    public class InvalidSrcLocationEvent : Event
    {
        public int Id { get; set; }
        public int? SrcLocation { get; set; }
        public int? ActualLocation { get; set; }
        public int DstLocation { get; set; }
    }

}