import React, { Component } from 'react'
// import { graphql } from 'react-apollo'
// import gql from 'graphql-tag'
import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'
import Link from './Link'

const LINKLIST_QUERY = gql`
query TransportUnits($articleId: Int, $locationId: Int) {
    transportUnits(articleId: $articleId, locationId: $locationId) {
      id
      barcode
      quantity
      location {
        id
        name
      }
      article {
        id
        code
        name
      }
    }
  }
`  

const LinkList = () => (
    <Query query={LINKLIST_QUERY}>
    {( {loading, error, data}) => {
      if (loading) return <div>Loading...</div>;
      if (error) return <div>Error</div>;
      return (
               <div>{data.transportUnits.map(link => <Link key={link.id} link={link} />)}</div>
             );
    }}
    </Query>
);

export default LinkList
// class LinkList extends Component {
//   render() {
//     // const linksToRender = [
//     //   {
//     //     id: '1',
//     //     description: 'Prisma turns your database into a GraphQL API 😎 😎',
//     //     url: 'https://www.prismagraphql.com',
//     //   },
//     //   {
//     //     id: '2',
//     //     description: 'The best GraphQL client',
//     //     url: 'https://www.apollographql.com/docs/react/',
//     //   },
//     // ]
//     if (this.props.feedQuery && this.props.feedQuery.loading) {
//         return <div>Loading</div>
//       }

//     if (this.props.feedQuery && this.props.feedQuery.error) {
//         return <div>Error</div>
//       }

//     const linksToRender = this.props.feedQuery.transportUnits


//     return (
//       <div>{linksToRender.map(link => <Link key={link.id} link={link} />)}</div>
//     )
//   }
// }

// const FEED_QUERY = gql`
// query TransportUnits($articleId: Int, $locationId: Int) {
//     transportUnits(articleId: $articleId, locationId: $locationId) {
//       id
//       barcode
//       quantity
//       location {
//         id
//         name
//       }
//       article {
//         id
//         code
//         name
//       }
//     }
//   }
// `  

// // 3
// //export default graphql(FEED_QUERY, { name: 'feedQuery' }) (LinkList)
