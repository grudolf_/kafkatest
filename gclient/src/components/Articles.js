import React  from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'

const QUERY = gql`
query Articles($articleId: Int, $code: String, $name: String) {
    articles(articleId: $articleId, code: $code, name: $name) {
        id
        code
        name
    }
  }
`  
const COLUMNS = [
    {
      dataField: 'id',
      text: 'Id',
      hidden: true,
    },
    {
      dataField: 'code',
      text: 'Code',
      sort: true,
    },
    {
      dataField: 'name',
      text: 'Name',
      sort: true,
    },
  ];

//function style, glej Locations za class
const Articles = () => (
    <Query query={QUERY}>
    {( {loading, error, data}) => {
      if (loading) return <div>Loading...</div>;
      if (error) return <div>Error</div>;
      return (
            //    <div>{data.transportUnits.map(link => <Link key={link.id} link={link} />)}</div>
               <div>
               <h1>Articles</h1>
               <p>This component demonstrates fetching data from the server.</p>
               <BootstrapTable
                 striped
                 hover
                 keyField="id"
                 data={data.articles}
                 columns={COLUMNS}
               />
             </div>
        );
    }}
    </Query>
);

export default Articles

// export default class Articles extends Component {
//   constructor() {
//     super();
//     this.state = {
//       articles: [],
//       columns: [
//         {
//           dataField: 'id',
//           text: 'Id',
//           hidden: true,
//         },
//         {
//           dataField: 'code',
//           text: 'Code',
//           sort: true,
//         },
//         {
//           dataField: 'name',
//           text: 'Name',
//           sort: true,
//         },
//       ],
//     };
//   }

//   componentDidMount() {
//     fetch('http://localhost:55742/api/Articles')
//       .then(response => response.json())
//       .then((data) => {
//         this.setState({ articles: data.articles });
//       });
//   }

//   render() {
//     return (
//       <div>
//         <h1>Articles</h1>
//         <p>This component demonstrates fetching data from the server.</p>
//         <BootstrapTable
//           striped
//           hover
//           keyField="id"
//           data={this.state.articles}
//           columns={this.state.columns}
//         />
//       </div>
//     );
//   }
// }
