// @flow
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Typeahead } from 'react-bootstrap-typeahead';
import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'

const QUERY = gql`
query Articles($articleId: Int, $code: String, $name: String) {
    articles(articleId: $articleId, code: $code, name: $name) {
        id
        code
        name
    }
  }
`

export default class ArticleSelector extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            options: [
                { id: '1', code: 'A000', name: 'Article 000' },
                { id: '2', code: 'A001', name: 'Article 001' },
                { id: '3', code: 'A002', name: 'Article 002' },
            ],
        };
    }

    // componentDidMount() {
    //     fetch('http://localhost:55742/api/Articles')
    //         .then(response => response.json())
    //         .then((data) => {
    //             this.setState({ options: data.locations });
    //         });
    // }

    handleChange(selected) {
        this.props.onChange(selected.selected);
    }

    render() {
        return (
            <Query query={QUERY}>
                {({ loading, error, data }) => {
                    if (loading) return <div>Loading...</div>;
                    if (error) return <div>Error</div>;
                    return (
                        <Fragment>
                            <Typeahead
                                labelKey={(option) => `${option.code} ${option.name}`}
                                onChange={selected => this.handleChange({ selected })}
                                options={data.articles}
                                selected={this.props.selected}
                                placeholder="Select an article"
                            />
                        </Fragment>
                    )
                }}
            </Query>
        );
    }
}

ArticleSelector.propTypes = {
    selected: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
    })),
    onChange: PropTypes.func.isRequired,
};
