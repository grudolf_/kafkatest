// @flow
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Typeahead } from 'react-bootstrap-typeahead';
import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'

const QUERY = gql`
query Locations($locationId: Int, $name: String) {
    locations(locationId: $locationId, name: $name) {
        id
        name
    }
  }
`

export default class LocationSelector extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            options: [
                { id: 1, name: 'Location 001' },
                { id: 2, name: 'Location 002' },
                { id: 3, name: 'Location 003' },
            ],
        };
    }

    // componentDidMount() {
    //     fetch('http://localhost:55742/api/Locations')
    //         .then(response => response.json())
    //         .then((data) => {
    //             this.setState({ options: data.locations });
    //         });
    // }

    handleChange(selected) {
        this.props.onChange(selected.selected);
    }

    render() {
        return (
            <Query query={QUERY}>
                {({ loading, error, data }) => {
                    if (loading) return <div>Loading...</div>;
                    if (error) return <div>Error</div>;
                    return (
                        <Fragment>
                            <Typeahead
                                labelKey="name"
                                onChange={selected => this.handleChange({ selected })}
                                options={data.locations}
                                selected={this.props.selected}
                                placeholder="Select a location"
                            />
                        </Fragment>
                    )
                }}
            </Query>
        );
    }
}

LocationSelector.propTypes = {
    selected: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
    })),
    onChange: PropTypes.func.isRequired,
};
