import React, { Component } from 'react';
import { Button, Form, FormGroup, ControlLabel } from 'react-bootstrap';
import TransportUnitMove from './TransportUnitMove';
import ArticleSelector from './ArticleSelector';
import LocationSelector from './LocationSelector';
import BootstrapTable from 'react-bootstrap-table-next';
import { gql } from 'apollo-boost'
import { withApollo } from 'react-apollo'

const QUERY = gql`
query TransportUnits($articleId: Int, $locationId: Int) {
    transportUnits(articleId: $articleId, locationId: $locationId) {
      id
      barcode
      quantity
      location {
        id
        name
      }
      article {
        id
        code
        name
      }
    }
  }
`

const COLUMNS = [
  {
    dataField: 'id',
    text: 'Id',
    hidden: true,
  },
  {
    dataField: 'barcode',
    text: 'Barcode',
    sort: true,
  },
  {
    dataField: 'article.code',
    text: 'Art. code',
    sort: true,
  },
  {
    dataField: 'article.name',
    text: 'Article',
    sort: true,
  },
  {
    dataField: 'location.name',
    text: 'Location',
    sort: true,
  },
  {
    dataField: 'quantity',
    text: 'Quantity',
    sort: true,
  },
];

class TransportUnits extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedArticle: [],
      selectedLocation: [],
      transportUnits: [],
    };
    this.handleArticle = this.handleArticle.bind(this);
    this.handleLocation = this.handleLocation.bind(this);
    this.handleRefresh = this.handleRefresh.bind(this);
  }

  handleArticle(selectedArticle) {
    this.setState({ selectedArticle });
  }

  handleLocation(selectedLocation) {
    this.setState({ selectedLocation });
  }

  async handleRefresh() {
    const variables = {};
    if (this.state.selectedArticle.length !== 0) {
      variables.articleId = this.state.selectedArticle[0].id;
    }
    if (this.state.selectedLocation.length !== 0) {
      variables.locationId = this.state.selectedLocation[0].id;
    }
    const result = await this.props.client.query({
      query: QUERY,
      variables: variables,
      fetchPolicy: 'network-only',  //mimo cache
    });
    const transportUnits = result.data.transportUnits;
    this.setState({ transportUnits })
  }

  render() {
    const rowEvents = {
      onClick: (e, row, rowIndex) => {
        this.setState({ selected: row });
      },
    };

    return (
      <div>
        <h1>Transport units</h1>
        <p>This component demonstrates fetching filtered data from the server.</p>
        <Form>
          <FormGroup>
            <ControlLabel>Article</ControlLabel>
            <ArticleSelector onChange={this.handleArticle} selected={this.selectedArticle} />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Location</ControlLabel>
            <LocationSelector onChange={this.handleLocation} selected={this.selectedLocation} />
          </FormGroup>
          <Button bsStyle="primary" onClick={this.handleRefresh}>Refresh</Button>
        </Form>
        <br />
        <BootstrapTable
          striped
          hover
          keyField="id"
          data={this.state.transportUnits}
          columns={COLUMNS}
          rowEvents={rowEvents}
        />
        {this.state.selected && this.state.selected.id ? (
          <TransportUnitMove transportUnit={this.state.selected} />
        ) : (<p>No selection</p>)}
      </div>
    );
  }
}

export default withApollo(TransportUnits);

