import React, { Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'

const QUERY = gql`
query Locations($locationId: Int, $name: String) {
    locations(locationId: $locationId, name: $name) {
        id
        name
    }
  }
`  
const COLUMNS = [
    {
      dataField: 'id',
      text: 'Id',
      hidden: true,
    },
    {
      dataField: 'name',
      text: 'Name',
      sort: true,
    },
  ];

export default class Locations extends Component {  
  render() {  
    return (
    <Query query={QUERY}>
    {( {loading, error, data}) => {
      if (loading) return <div>Loading...</div>;
      if (error) return <div>Error</div>;
      return (
            //    <div>{data.transportUnits.map(link => <Link key={link.id} link={link} />)}</div>
               <div>
               <h1>Locations</h1>
               <p>This component demonstrates fetching data from the server.</p>
               <BootstrapTable
                 striped
                 hover
                 keyField="id"
                 data={data.locations}
                 columns={COLUMNS}
               />
             </div>
        );
    }}
    </Query>
    );
  }
}

//export default Locations

