import React, { Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import TransportUnitMove from './TransportUnitMove';

/*
Ločena komponenta za prikaz tabele, uporaba:

  <Query query={QUERY}>
    {({ loading, error, data }) => {
      if (loading) return <div>Loading...</div>;
      if (error) return <div>Error</div>;
      return <TransportUnitsTable transportUnits={data.transportUnits} />;
    }}
  </Query>

*/
const COLUMNS = [
  {
    dataField: 'id',
    text: 'Id',
    hidden: true,
  },
  {
    dataField: 'barcode',
    text: 'Barcode',
    sort: true,
  },
  {
    dataField: 'article.code',
    text: 'Art. code',
    sort: true,
  },
  {
    dataField: 'article.name',
    text: 'Article',
    sort: true,
  },
  {
    dataField: 'location.name',
    text: 'Location',
    sort: true,
  },
  {
    dataField: 'quantity',
    text: 'Quantity',
    sort: true,
  },
];

export default class TransportUnitsTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transportUnits: props.transportUnits,
      selected: null,

    };
  }

  render() {
    const rowEvents = {
      onClick: (e, row, rowIndex) => {
        this.setState({ selected: row });
      },
    };

    return (
      <div>
        <h1>Transport units</h1>
        <p>This component demonstrates fetching data from the server.</p>
        <BootstrapTable
          striped
          hover
          keyField="id"
          data={this.state.transportUnits}
          columns={COLUMNS}
          rowEvents={rowEvents}
        />
        {this.state.selected && this.state.selected.id ? (
          <TransportUnitMove transportUnit={this.state.selected} />
        ) : (<p>No selection</p>)}       </div>
    );
  }
}



