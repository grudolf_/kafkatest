import React, { Component } from 'react';
import { Route } from 'react-router';
import './App.css';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import Articles from './components/Articles';
import Locations from './components/Locations';
import TransportUnits from './components/TransportUnits';
//import LinkList from './components/LinkList';

class App extends Component {
  render() {
//    return <LinkList />
    return (
    <Layout>
    <Route exact path="/" component={Home} />
    <Route path="/articles" component={Articles} />
    <Route path="/locations" component={Locations} />
    <Route path="/transportUnits" component={TransportUnits} />
  </Layout>
    );
}
}

export default App;
