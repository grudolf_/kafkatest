﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;

namespace SimpleProducer
{
    class Program
    {
        static void Main(string[] args)
        {
            string kafkaEndpoint = "127.0.0.1:29092";

            // The Kafka topic we'll be using
            string kafkaTopic = "wms.cmd";

            // Create the producer configuration
            var producerConfig = new Dictionary<string, object>
            {
                { "bootstrap.servers", kafkaEndpoint },
                { "queued.min.messages", 1 },
                {"socket.blocking.max.ms", "5"}, //min=1
            };

            // Create the producer
            using (var producer = new Producer<Null, string>(producerConfig, null, new StringSerializer(Encoding.UTF8)))
            {
                // Send 10 messages to the topic
                for (int i = 0; i < 10; i++)
                {
                    var message = $"Event {i} at {DateTime.Now}";
                    var result = producer.ProduceAsync(kafkaTopic, null, message).GetAwaiter().GetResult();
                    Console.WriteLine($"Event {i} sent on Partition: {result.Partition} with Offset: {result.Offset}");
                }
            }

            using (var producer = new Producer<Null, string>(producerConfig, null, new StringSerializer(Encoding.UTF8)))
            {
                // Send 10 messages to the topic
                for (int i = 10; i < 20; i++)
                {
                    var message = $"Event {i} at {DateTime.Now}";
                    producer.ProduceAsync(kafkaTopic, null, message).ContinueWith(task =>
                    {
                        Console.WriteLine(
                            $"Event {task.Result.Value} sent on Partition: {task.Result.Partition} with Offset: {task.Result.Offset}");
                    });
                }
                producer.Flush(-1);
            }


            //            using (var producer = new Producer<string, string>(producerConfig, null, new StringSerializer(Encoding.UTF8)))
            using (var producer = new Producer(producerConfig, false, false))
            {
                Console.WriteLine($"Sending 10 more");
                for (int i = 20; i < 30; i++)
                {
                    var key = "test";
                    var message = $"Event {i}";
                    //var result = producer.ProduceAsync(kafkaTopic, Encoding.UTF8.GetBytes(key), Encoding.UTF8.GetBytes(message)).GetAwaiter().GetResult();
                    var result = producer.ProduceAsync(kafkaTopic, Encoding.UTF8.GetBytes(key), Encoding.UTF8.GetBytes(message));
                }
                Console.WriteLine($"Sent 10 more");

                producer.Flush(-1);

            }

                Console.WriteLine("Any key...");
            Console.ReadKey();
        }
    }
}
