## Kafka v Docker containerjih

Testiranje na (Windows?) localhost:

Docker-compose.yml je nastavljen tako, da je kafka od zunaj dosegljiva na localhost:29092. Preveri KAFKA_ADVERTISED_LISTENERS.


Dvigni in pripravi topic:

```
docker-compose up -d
docker-compose ps
#ko kafka in zookeeper delujeta, pripravi topice
docker-compose exec kafka kafka-topics --create --topic foo --partitions 1 --replication-factor 1 --if-not-exists --zookeeper zookeeper:32181
docker-compose exec kafka kafka-topics --create --topic wms.cmd --partitions 1 --replication-factor 1 --if-not-exists --zookeeper zookeeper:32181
docker-compose exec kafka kafka-topics --create --topic wms.event --partitions 1 --replication-factor 1 --if-not-exists --zookeeper zookeeper:32181
```

Ko končaš, ugasni z `docker-compose down`

Uporabljamo dva topica - v **wms.cmd** pošiljamo ukaze, ki jih nekdo obdela, v **wms.event** pa evente

## .NET

Zahteva .Net Core 2.1, za bazo podatkov uporablja LocalDb.

Zagon vseh servisov s `start.ps1`.

Struktura:

- WMS.API - API backend
    - read-only? dostop do baze
    - sinhrona komunikacija s klienti preko http://localhost:55742
    - API test/dokumentacija na http://localhost:55742/swagger
    - wms.cmd producer skozi TransportUnits/Move

- WMS.BackendWorker
    - read+write dostop do baze
    - consumer wms.cmd, producer wms.event

- QueLog - auditing, consumer za wmc.cmd in wms.event
- WMS.Msg - definicije sporočil, deserializacija za producerje in consumerje
- WMS.Database - baza

### GraphQL

.NET projekt je WMS.GraphQL, uporablja WMS.Database. UI je dostopen na http://localhost:60306/graphql/ , ločen zagon

Uporablja GraphQL iz https://github.com/graphql-dotnet/graphql-dotnet kot .NET Implementacijo in GraphiQL (UI). Resolver je omejen, problem imamo s SQL N+1 branji ali nepotrebnimi branji podatkov, ki jih uporabnik ni zahteval.
Alternativa je https://github.com/ckimes89/graphql-net , ki podpira IQueryable, ima pa zato kup drugih omejitev in počasen razvoj.

- podobno OData, ki ima več podpore za .Net je pa bolj ali manj mrtva v ostalih okoljih?
- Read stran mogoče? zmanjša število REST GET endpointov oz. prenos neželjenih podatkov k klientu. Precej ročnega dela za mapiranje model -> view model. 
- Uporaba za Write stran? 
- Avtentikacija uporablja standard (cookie, JWT,..) kako je z avtorizacijo?
- Preskusi uporabo iz javascript klienta.



## React client

Za zagon zahteva nodejs in npm, v produkciji pristanejo samo static files. Ajax komunikacija z WMS.API.

```
npm install
#dev start:
npm start
#prod.build:
npm run-script build
npm install -g serve
serve -s build
```

