import React, { Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';

export default class Locations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locations: [],
      columns: [
        {
          dataField: 'id',
          text: 'Id',
          hidden: true,
        },
        {
          dataField: 'name',
          text: 'Name',
          sort: true,
        },
      ],
    };
  }

  componentDidMount() {
    fetch('http://localhost:55742/api/Locations')
      .then(response => response.json())
      .then((data) => {
        this.setState({ locations: data.locations });
      });
  }

  render() {
    return (
      <div>
        <h1>Locations</h1>
        <p>This component demonstrates fetching data from the server.</p>
        <BootstrapTable
          striped
          hover
          keyField="id"
          data={this.state.locations}
          columns={this.state.columns}
        />
      </div>
    );
  }
}
