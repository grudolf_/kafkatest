import React, { Component } from 'react';
import { Button, Form, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import ArticleSelector from './ArticleSelector';
import LocationSelector from './LocationSelector';
import TransportUnitMove from './TransportUnitMove';

export default class TransportUnits extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedArticle: [],
      selectedLocation: [],
      transportUnits: [],
      columns: [
        {
          dataField: 'id',
          text: 'Id',
          hidden: true,
        },
        {
          dataField: 'barcode',
          text: 'Barcode',
          sort: true,
        },
        {
          dataField: 'articleCode',
          text: 'Art. code',
          sort: true,
        },
        {
          dataField: 'articleName',
          text: 'Article',
          sort: true,
        },
        {
          dataField: 'locationName',
          text: 'Location',
          sort: true,
        },
        {
          dataField: 'quantity',
          text: 'Quantity',
          sort: true,
        },
      ],
    };
    this.handleArticle = this.handleArticle.bind(this);
    this.handleLocation = this.handleLocation.bind(this);
    this.handleRefresh = this.handleRefresh.bind(this);
  }

  handleRefresh() {
    const params = {};
    if (this.state.selectedArticle.length !== 0) {
      params.articleId = this.state.selectedArticle[0].id;
    }
    if (this.state.selectedLocation.length !== 0) {
      params.locationId = this.state.selectedLocation[0].id;
    }
    const url = new URL('http://localhost:55742/api/TransportUnits');
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    fetch(url)
      .then(response => response.json())
      .then((data) => {
        this.setState({ transportUnits: data.transportUnits, selected: null });
      });
  }

  handleArticle(selectedArticle) {
    this.setState({ selectedArticle });
  }

  handleLocation(selectedLocation) {
    this.setState({ selectedLocation });
  }

  render() {
    // const selectRow = {
    //   mode: 'checkbox',
    //   clickToSelect: true,
    // };

    const rowEvents = {
      onClick: (e, row, rowIndex) => {
        this.setState({ selected: row });
      },
    };

    return (
      <div>
        <h1>Transport units</h1>
        <p>This component demonstrates fetching data from the server.</p>
        <Form>
          <FormGroup>
            <ControlLabel>Article</ControlLabel>
            <ArticleSelector onChange={this.handleArticle} selected={this.selectedArticle} />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Location</ControlLabel>
            <LocationSelector onChange={this.handleLocation} selected={this.selectedLocation} />
          </FormGroup>
          <Button bsStyle="primary" onClick={this.handleRefresh}>Refresh</Button>
        </Form>
        <BootstrapTable
          striped
          hover
          keyField="id"
          data={this.state.transportUnits}
          columns={this.state.columns}
          // selectRow={selectRow}
          rowEvents={rowEvents}
        />
        {this.state.selected && this.state.selected.id ? (
          <TransportUnitMove transportUnit={this.state.selected} />
          // <Form>
          //   <FormGroup>
          //     <ControlLabel>Transport unit</ControlLabel>
          //     <FormControl.Static>{this.state.selected.id}</FormControl.Static>
          //   </FormGroup>
          // </Form>
        ) : (<p>No selection</p>)}
      </div>
    );
  }
}
