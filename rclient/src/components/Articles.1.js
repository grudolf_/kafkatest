import React, { Component } from 'react';

export class Articles extends Component {
  displayName = Articles.name

  constructor(props) {
    super(props);
    this.state = { articles: [], loading: true };

    fetch('http://localhost:55742/api/Articles')
      .then(response => response.json())
      .then(data => {
        this.setState({ articles: data.articles, loading: false });
      });
  }

  static renderArticleTable(articles) {
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>Id</th>
            <th>Code</th>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {articles.map(article =>
            <tr key={article.id}>
              <td>{article.id}</td>
              <td>{article.code}</td>
              <td>{article.name}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : Articles.renderArticleTable(this.state.articles);

    return (
      <div>
        <h1>Articles</h1>
        <p>This component demonstrates fetching data from the server.</p>
        {contents}
      </div>
    );
  }
}
