import React, { Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';

export default class Articles extends Component {
  constructor() {
    super();
    this.state = {
      articles: [],
      columns: [
        {
          dataField: 'id',
          text: 'Id',
          hidden: true,
        },
        {
          dataField: 'code',
          text: 'Code',
          sort: true,
        },
        {
          dataField: 'name',
          text: 'Name',
          sort: true,
        },
      ],
    };
  }

  componentDidMount() {
    fetch('http://localhost:55742/api/Articles')
      .then(response => response.json())
      .then((data) => {
        this.setState({ articles: data.articles });
      });
  }

  render() {
    return (
      <div>
        <h1>Articles</h1>
        <p>This component demonstrates fetching data from the server.</p>
        <BootstrapTable
          striped
          hover
          keyField="id"
          data={this.state.articles}
          columns={this.state.columns}
        />
      </div>
    );
  }
}
