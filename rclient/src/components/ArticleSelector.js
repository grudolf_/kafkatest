import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Typeahead } from 'react-bootstrap-typeahead';

export default class ArticleSelector extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      options: [
        { id: '1', code: 'A000', name: 'Article 000' },
        { id: '2', code: 'A001', name: 'Article 001' },
        { id: '3', code: 'A002', name: 'Article 002' },
      ],
    };
  }

  componentDidMount() {
    fetch('http://localhost:55742/api/Articles')
      .then(response => response.json())
      .then((data) => {
        this.setState({ options: data.articles });
      });
  }

  handleChange(selected) {
    this.props.onChange(selected.selected);
  }
  /*
        <Typeahead
          labelKey={(option) => `${option.code} ${option.name}`}
          onChange={(selected) => this.setState({selected})}
          options={this.state.options}
          selected={this.state.selected}
          placeholder='Select an article'/>
  */
  render() {
    return (
      <Fragment>
        <Typeahead
          labelKey={option => `${option.code} ${option.name}`}
          onChange={selected => this.handleChange({ selected })}
          options={this.state.options}
          selected={this.props.selected}
          placeholder="Select an article"
        />
      </Fragment>
    );
  }
}

ArticleSelector.propTypes = {
  selected: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    code: PropTypes.string,
    name: PropTypes.string,
  })),
  onChange: PropTypes.func.isRequired,
};
