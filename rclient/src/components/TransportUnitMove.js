import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import LocationSelector from './LocationSelector';

export default class TransportUnitMove extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedLocation: [],
    };
    this.handleLocation = this.handleLocation.bind(this);
    this.handleMove = this.handleMove.bind(this);
  }

  handleLocation(selectedLocation) {
    this.setState({ selectedLocation });
  }

  handleMove() {
    if (this.state.selectedLocation.length === 0) { return; }
    const loc = this.props.transportUnit;
    const url = new URL('http://localhost:55742/api/TransportUnits/move');
    fetch(url, {
      crossDomain: true,
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        id: loc.id,
        srcLocation: loc.locationId,
        dstLocation: this.state.selectedLocation[0].id,
      }),
    })
      .then(response => response.json())
      .then((data) => console.log(data));
  }

  render() {
    return (
      <Form>
        <FormGroup>
          <ControlLabel>Transport unit</ControlLabel>
          <FormControl.Static>{this.props.transportUnit.barcode} ({this.props.transportUnit.id})</FormControl.Static>
        </FormGroup>
        <FormGroup>
          <ControlLabel>Article</ControlLabel>
          <FormControl.Static>{this.props.transportUnit.articleCode} {this.props.transportUnit.articleName}, Quantity: {this.props.transportUnit.quantity}</FormControl.Static>
        </FormGroup>
        <FormGroup>
          <ControlLabel>Current location</ControlLabel>
          <FormControl.Static>{this.props.transportUnit.locationName}</FormControl.Static>
        </FormGroup>
        <FormGroup>
          <ControlLabel>New location</ControlLabel>
          <LocationSelector onChange={this.handleLocation} selected={this.selectedLocation} />
        </FormGroup>
        <Button bsStyle="primary" onClick={this.handleMove}>Create movement order</Button>
      </Form>
    );
  }
}

TransportUnitMove.propTypes = {
  transportUnit: PropTypes.shape({
    id: PropTypes.number,
    barcode: PropTypes.string,
    articleId: PropTypes.number,
    articleCode: PropTypes.string,
    articleName: PropTypes.string,
    locationId: PropTypes.number,
    locationName: PropTypes.string,
    quantity: PropTypes.number,
  }).isRequired,
};
