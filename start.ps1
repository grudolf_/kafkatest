# Start API and workers
# Fix execution policy:
# set-executionpolicy -Scope CurrentUser remotesigned
Start-Process -WorkingDirectory WMS.API -FilePath "dotnet.exe" "run"
Start-Process "http://localhost:55742/swagger/"
Start-Process -WorkingDirectory QueLog -FilePath "dotnet.exe" "run"
Start-Process -WorkingDirectory WMS.BackendWorker -FilePath "dotnet.exe" "run"
# druga instanca workerja:
#Start-Process -WorkingDirectory WMS.BackendWorker -FilePath "dotnet.exe" "run --no-build"
