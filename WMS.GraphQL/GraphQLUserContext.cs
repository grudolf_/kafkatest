﻿using System.Security.Claims;

namespace WMS.GraphQL
{
    public class GraphQLUserContext
    {
        public ClaimsPrincipal User { get; set; }
    }
}