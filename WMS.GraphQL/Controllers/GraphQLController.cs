﻿//using System;
//using System.Threading.Tasks;
//using GraphQL;
//using GraphQL.DataLoader;
//using GraphQL.Types;
//using Microsoft.AspNetCore.Mvc;
//using WMS.GraphQL.Models;
//
//namespace WMS.GraphQL.Controllers
//{
//    [Route("[controller]")]
//    [ApiController]
//    public class GraphQLController : ControllerBase
//    {
//        private readonly IDocumentExecuter _documentExecuter;
//        private readonly DataLoaderDocumentListener _dataLoaderDocumentListener;
//        private readonly ISchema _schema;
//
//        public GraphQLController(ISchema schema, IDocumentExecuter documentExecuter, DataLoaderDocumentListener dataLoaderDocumentListener)
//        {
//            _schema = schema;
//            _documentExecuter = documentExecuter;
//            _dataLoaderDocumentListener = dataLoaderDocumentListener;
//        }
//
//        [HttpPost]
//        public async Task<IActionResult> Post([FromBody] GraphQLQuery query)
//        {
//            if (query == null) { throw new ArgumentNullException(nameof(query)); }
//            var inputs = query.Variables.ToInputs();
//            var result = await _documentExecuter.ExecuteAsync(opts =>
//            {
//                opts.Schema = _schema;
//                opts.Query = query.Query;
//                opts.Inputs = inputs;
//                opts.Listeners.Add(_dataLoaderDocumentListener);
//            }).ConfigureAwait(false);
//            //var result = await DataLoaderContext.Run() _documentExecuter.ExecuteAsync(executionOptions).ConfigureAwait(false);
//
//            if (result.Errors?.Count > 0)
//            {
//                return BadRequest(result);
//            }
//
//            return Ok(result);
//        }
//    }
//}
