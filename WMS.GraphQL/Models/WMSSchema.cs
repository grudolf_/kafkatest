﻿using System;
using System.Linq;
using GraphQL;
using GraphQL.DataLoader;
using GraphQL.Types;
using Microsoft.EntityFrameworkCore;
using WMS.Database;
using WMS.Database.Models;

namespace WMS.GraphQL.Models
{
    public class WMSSchema : Schema
    {
        public WMSSchema(IDependencyResolver resolver): base(resolver)
        {
            Query = resolver.Resolve<WMSQuery>();
        }

//        public WMSSchema(IServiceProvider services, WMSQuery query) : base(new DependencyResolver(services))
//        {
//            Query = query;
//        }

    }

    public class DependencyResolver : IDependencyResolver
    {
        private readonly IServiceProvider _services;

        public DependencyResolver(IServiceProvider services)
        {
            _services = services;
        }

        public T Resolve<T>() => (T)_services.GetService(typeof(T));

        public object Resolve(Type type) => _services.GetService(type);
    }

    public class WMSQuery : ObjectGraphType
    {
        public WMSQuery(IDataLoaderContextAccessor accessor, WMSContext ctx)
        {
            Field<LocationType>("location",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id"}),
                resolve: context => ctx.Locations.Where(x => x.Id == context.GetArgument<int>("id",0)).FirstOrDefault());
            Field<ArticleType>("article",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context => ctx.Articles.Where(x => x.Id == context.GetArgument<int>("id", 0)).FirstOrDefault());
            Field<TransportUnitType>("transportUnit",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
                resolve: context => ctx.TransportUnits.Where(x => x.Id == context.GetArgument<int>("id", 0)).Include(x => x.Article).Include(x => x.Location).FirstOrDefault());

            /*
             * Primer uporabe: 
             query TransportUnits($articleId: Int, $locationId: Int)
               {transportUnits(articleId: $articleId, locationId: $locationId) 
               {id,barcode,quantity, location {id, name}, article {id, code, name}}
             }
               
             {
               "articleId": 2,
               "locationId": 4
              }
             */
            Field<ListGraphType<ArticleType>>("articles",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "articleId" }, new QueryArgument<StringGraphType> { Name = "code" }, new QueryArgument<StringGraphType> { Name = "name" }),
                resolve: context =>
                {
                    var q = ctx.Articles.AsQueryable();
                    if (context.HasArgument("articleId"))
                        q = q.Where(x => x.Id == context.GetArgument<int>("articleId", 0));
                    if (context.HasArgument("code"))
                        q = q.Where(x => EF.Functions.Like(x.Code, context.GetArgument<string>("code", "")));
                    if (context.HasArgument("name"))
                        q = q.Where(x => EF.Functions.Like(x.Name, context.GetArgument<string>("name", "")));
                    return q.ToListAsync();
                });
            Field<ListGraphType<LocationType>>("locations",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "locationId" }, new QueryArgument<StringGraphType> { Name = "name" }),
                resolve: context =>
                {
                    var q = ctx.Locations.AsQueryable();
                    if (context.HasArgument("locationId"))
                        q = q.Where(x => x.Id == context.GetArgument<int>("articleId", 0));
                    if (context.HasArgument("name"))
                        q = q.Where(x => EF.Functions.Like(x.Name, context.GetArgument<string>("name", "")));
                    return q.ToListAsync();
                });
            Field<ListGraphType<TransportUnitType>>("transportUnits",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "articleId" }, new QueryArgument<IntGraphType> { Name = "locationId" }),
                resolve: context =>
                {
                    var q = ctx.TransportUnits.AsQueryable();
                    if (context.HasArgument("articleId"))
                        q = q.Where(x => x.ArticleId == context.GetArgument<int>("articleId", 0));
                    if (context.HasArgument("locationId"))
                        q = q.Where(x => x.LocationId == context.GetArgument<int>("locationId", 0));
                    return q.ToListAsync();
                });
        }
    }

    public class LocationType : ObjectGraphType<Location>
    {
        public LocationType(WMSContext context)
        {
            Field(x => x.Id);
            Field(x => x.Name);
        }
    }
    public class ArticleType : ObjectGraphType<Article>
    {
        public ArticleType(WMSContext context)
        {
            Field(x => x.Id);
            Field(x => x.Code);
            Field(x => x.Name);
        }
    }
    public class TransportUnitType : ObjectGraphType<TransportUnit>
    {
        public TransportUnitType(IDataLoaderContextAccessor accessor, WMSContext ctx)
        {
            Field(x => x.Id);
            Field(x => x.Barcode);
            Field(x => x.Quantity);
            //Če delamo z Include ali z Lazy Loading za dostop zadostuje navigation property
            //Field<ArticleType>("article", resolve: context => context.Source.Article);
            //Field<LocationType>("location", resolve: context => context.Source.Location);
            //nalaganje preko Dataloader 
            Field<ArticleType, Article>("article").ResolveAsync(context =>
            {
                var loader = accessor.Context.GetOrAddBatchLoader<int, Article>("GetArticlesById", ctx.GetArticlesByIdAsync);
                if (context.Source.ArticleId == null)
                    return null;
                return loader.LoadAsync(context.Source.ArticleId.Value);
            });
            Field<LocationType, Location>("location").ResolveAsync(context =>
            {
                // Get or add a batch loader with the key "GetLocationsById"
                // The loader will call GetLocationsByIdAsync for each batch of keys
                var loader = accessor.Context.GetOrAddBatchLoader<int, Location>("GetLocationsById", ctx.GetLocationsByIdAsync);
                // Add this UserId to the pending keys to fetch
                // The task will complete once the GetLocationsByIdAsync() returns with the batched results
                if (context.Source.LocationId == null)
                    return null;
                return loader.LoadAsync(context.Source.LocationId.Value);
            });
        }
    }
}