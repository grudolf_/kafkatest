﻿using System;
using Microsoft.AspNetCore.Http;

namespace WMS.GraphQL
{
    public class GraphQLSettings
    {
        //public PathString Path { get; set; } = "/api/graphql";
        public PathString Path { get; set; } = "/graphql";
        public Func<HttpContext, object> BuildUserContext { get; set; }
    }
}