﻿namespace WMS.Database.Models
{
    public class TransportUnit
    {
        public int Id { get; set; }
        public string Barcode { get; set; }
        public int? ArticleId { get; set; }
        public int? LocationId { get; set; }
        public int Quantity { get; set; }

        public Article Article { get; set; }
        public Location Location { get; set; }
    }
}
