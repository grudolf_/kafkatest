﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace WMS.API
{
    public interface IKafkaClient
    {
        Task<Message<string, string>> Produce(string topic, string key, string value);
        Task<Message<string, string>> Produce(string topic, string key, object value);
        int Flush(int millisecondsTimeout);
    }

    public class KafkaClient : IKafkaClient
    {
        private Producer<string, string> _producer;

        public KafkaClient(IConfiguration globalConf)
        {
            var config = new Dictionary<string, object>
            {
                {"bootstrap.servers", globalConf.GetValue<string>("Kafka:BootstrapServers")},
                {"queued.min.messages", 1},
                {"socket.blocking.max.ms", "5"}, //min=1
            };

            _producer = new Producer<string, string>(config, new StringSerializer(Encoding.UTF8), new StringSerializer(Encoding.UTF8));
        }

        public async Task<Message<string, string>> Produce(string topic, string key, string value)
        {
            return await _producer.ProduceAsync(topic, key, value);
        }

        public async Task<Message<string, string>> Produce(string topic, string key, object value)
        {
            var ser = JsonConvert.SerializeObject(value, Formatting.None);
            return await _producer.ProduceAsync(topic, key, ser);
        }

        public int Flush(int millisecondsTimeout)
        {
            return _producer.Flush(millisecondsTimeout);
        }
    }
}