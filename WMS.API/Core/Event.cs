﻿using System;
using MediatR;

namespace WMS.API.Core
{
    public abstract class Event<T> : Message<T>, INotification
    {
        public DateTime Timestamp { get; private set; }

        protected Event()
        {
            Timestamp = DateTime.Now;
        }
    }
}