﻿using MediatR;

namespace WMS.API.Core
{
    public abstract class Message<T> : IRequest<T>
    {
        public string MessageType { get; protected set; }

        protected Message()
        {
            MessageType = GetType().FullName;
        }
    }
}