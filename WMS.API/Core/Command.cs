﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WMS.API.Core
{
    public interface ICommand
    {
        string MessageType { get; }
        DateTime Timestamp { get; }
        ValidationResult ValidationResult { get; set; }
        bool IsValid();
    }

    public abstract class Command<T> : Message<T>, ICommand
    {
        public DateTime Timestamp { get; private set; }
        public ValidationResult ValidationResult { get; set; }

        protected Command()
        {
            Timestamp = DateTime.Now;
        }

        public abstract bool IsValid();
    }

    public abstract class Response<T> where T:ICommand
    {
        public string MessageType { get; protected set; }
        public DateTime Timestamp { get; private set; }
        public string RequestType { get; protected set; }
        public DateTime RequestTimestamp { get; private set; }

        private Response() { }
//        protected Response()
//        {
//            Timestamp = DateTime.Now;
//            MessageType = GetType().FullName;
//        }

        internal Response(ICommand request)
        {
            MessageType = GetType().FullName;
            Timestamp = DateTime.Now;
            RequestType = request.MessageType;
            RequestTimestamp = request.Timestamp;
        }

    }
}