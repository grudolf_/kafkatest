﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.EntityFrameworkCore;
//using WMSBackend.Models;
//
//namespace WMSBackend
//{
//    public class WMSContext : DbContext
//    {
//        public WMSContext(DbContextOptions options) : base(options)
//        {
//        }
//
//        public DbSet<Location> Locations { get; set; }
//        public DbSet<Article> Articles { get; set; }
//        public DbSet<TransportUnit> TransportUnits { get; set; }
//
//        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        //        {
//        //            optionsBuilder.UseSqlite("Data Source=wms.db");
//        //        }
//    }
//
//    public static class WMSContextExtensions
//    {
//        public static void EnsureSeedData(this WMSContext context)
//        {
//            if (!context.Locations.Any())
//            {
//                var rand = new Random(42);
//
//                const int locationCount = 10;
//                const int articlesCount = 10;
//                const int transportUnitsCount = 500;
//
//                var locations = new List<Location>(locationCount);
//                var articles = new List<Article>(articlesCount);
//                var transportUnits = new List<TransportUnit>(transportUnitsCount);
//                for (int i = 0; i < locationCount; i++)
//                    locations.Add(new Location {Id = Guid.NewGuid(), Name = $"L{i:D6}"});
//                for (int i = 0; i < articlesCount; i++)
//                    articles.Add(new Article {Id = Guid.NewGuid(), Code = $"A{i:D3}", Name = $"Article {i:D3}"});
//
//                using (context.Database.BeginTransaction())
//                {
//                    context.Locations.AddRange(locations);
//                    context.Articles.AddRange(articles);
//                    context.SaveChanges();
//
//                    for (int i = 0; i < transportUnitsCount; i++)
//                    {
//                        var randLocation = locations[rand.Next(0, locationCount)];
//                        var randArticle = articles[rand.Next(0, locationCount)];
//                        context.TransportUnits.Add(new TransportUnit
//                        {
//                            Article = randArticle,
//                            Location = randLocation,
//                            Barcode = $"{randArticle.Code}{i:D5}",
//                            Quantity = rand.NextDouble() < 0.75 ? 100 : rand.Next(1, 100)
//                        });
//                        if (i % 100 == 0)
//                            context.SaveChanges();
//                    }
//
//                    context.SaveChanges();
//                    context.Database.CommitTransaction();
//                }
//
//            }
//        }
//    }
//}