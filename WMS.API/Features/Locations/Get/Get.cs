﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WMS.API.Core;
using WMS.Database;
using WMS.Database.Models;

namespace WMS.API.Features.Locations.Get
{
    public class Query : Command<Result>    // IRequest<Result>
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        public override bool IsValid()
        {
            return true;
        }
    }

    public class Result : Response<Query>
    {
        public Result(Query request) : base(request) { }

        public Location Location { get; set; }
    }

    public class Handler : IRequestHandler<Query, Result>
    {
        private readonly WMSContext _context;

        public Handler(WMSContext context)
        {
            _context = context;
        }

        public async Task<Result> Handle(Query request, CancellationToken cancellationToken)
        {
            var qry = _context.Locations.AsQueryable();
            if (request.Id != null)
                qry = qry.Where(x => x.Id == request.Id);
            if (request.Name != null)
                qry = Queryable.Where(qry, x => x.Name == request.Name);
            var location = await qry.FirstOrDefaultAsync(cancellationToken);
            return new Result(request)
            {
                Location = location
            };
        }
    }
}
