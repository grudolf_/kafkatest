﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WMS.API.Core;
using WMS.Database;
using WMS.Database.Models;

namespace WMS.API.Features.Locations.Index
{
    public class Query : Command<Result> // IRequest<Result>
    {
        public override bool IsValid()
        {
            return true;
        }
    }

    public class Result : Response<Query>
    {
        public Result(Query request) : base(request)
        {
        }

        public List<Location> Locations { get; set; }
    }

    public class Handler : IRequestHandler<Query, Result>
    {
        private readonly WMSContext _context;

        public Handler(WMSContext context)
        {
            _context = context;
        }

        public async Task<Result> Handle(Query request, CancellationToken cancellationToken)
        {
            var articles = await _context.Locations.OrderBy(x => x.Name).ToListAsync(cancellationToken);
            return new Result(request)
            {
                Locations = articles
            };
        }
    }
}