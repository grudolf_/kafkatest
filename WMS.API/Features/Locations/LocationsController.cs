﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WMS.API.Features.Locations
{
    [Produces("application/json")]
    [Route("api/Locations")]
    public class LocationsController : Controller
    {
        private readonly IMediator _mediator;

        public LocationsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET: api/Locations
        [HttpGet]
        public async Task<Index.Result> Get()
        {
            var result = await _mediator.Send(new Index.Query());
            return result;
        }

        // GET: api/Locations/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<Get.Result> Get(Get.Query query)
        {
            var result = await _mediator.Send(query);
            return result;
        }

        // POST: api/Locations
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Locations/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

