﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using WMS.API.Features.TransportUnits.Move;

namespace WMS.API.Features.TransportUnits
{
    [Produces("application/json")]
    [Route("api/TransportUnits")]
    public class TransportUnitsController : Controller
    {
        private readonly IMediator _mediator;

        public TransportUnitsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<Index.Result> Index(Index.Query query)
        {
            var result = await _mediator.Send(query);
            return result;
        }

        [HttpPost]
        [Route("move")]
        public async Task<string> Move([FromBody] Command command)
        {
            await _mediator.Publish(command);
            return command.Result;
        }
    }
}

