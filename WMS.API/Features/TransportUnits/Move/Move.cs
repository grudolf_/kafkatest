﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using WMS.Msg;

namespace WMS.API.Features.TransportUnits.Move
{
    public class Command : INotification
    {
        public int Id { get; set; }
        public int? SrcLocation { get; set; }
        public int DstLocation { get; set; }
        public string Result { get; set; }
    }

    public class Handler : INotificationHandler<Command>
    {
        private readonly IKafkaClient _kafkaClient;

        public Handler(IKafkaClient kafkaClient)
        {
            _kafkaClient = kafkaClient;
        }

        Task INotificationHandler<Command>.Handle(Command cmd, CancellationToken cancellationToken)
        {
            //TODO: validacija

            var outCmd = new WMS.Msg.TransportUnits.MoveCommand
            {
                Id = cmd.Id,
                SrcLocation = cmd.SrcLocation,
                DstLocation = cmd.DstLocation,
            };

            Dictionary<string, string> headers = new Dictionary<string, string>(){
                { Headers.Type, outCmd.GetType().FullName },
                { Headers.MessageId, Guid.NewGuid().ToString() },
                { Headers.CorrelationId,  Guid.NewGuid().ToString()},
                { Headers.DateCreated, DateTime.Now.ToString(CultureInfo.InvariantCulture) },
            };
            var msg = new Message(headers, outCmd);
            //_kafkaClient.Produce("foo", outCmd.Id.ToString(), outCmd);
            _kafkaClient.Produce("wms.cmd", outCmd.Id.ToString(), msg);
            _kafkaClient.Flush(50);
            cmd.Result = "Created command " + headers[Headers.MessageId];
            return Task.CompletedTask;
        }
    }
}