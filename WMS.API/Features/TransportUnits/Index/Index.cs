﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WMS.API.Core;
using WMS.Database;

namespace WMS.API.Features.TransportUnits.Index
{
    public class Query : Command<Result> // IRequest<Result>
    {
        public int? ArticleId { get; set; }
        public int? LocationId { get; set; }

        public override bool IsValid()
        {
            return true;
        }
    }

    public class Result : Response<Query>
    {
        public Result(Query request) : base(request)
        {
        }

        public List<Model> TransportUnits { get; set; }
    }

    public class Model
    {
        public int Id { get; set; }
        public string Barcode { get; set; }
        public int? ArticleId { get; set; }
        public string ArticleCode { get; set; }
        public string ArticleName { get; set; }
        public int? LocationId { get; set; }
        public string LocationName { get; set; }
        public int Quantity { get; set; }
    }


    public class Handler : IRequestHandler<Query, Result>
    {
        private readonly WMSContext _context;

        public Handler(WMSContext context)
        {
            _context = context;
        }

        public async Task<Result> Handle(Query request, CancellationToken cancellationToken)
        {
            var qry = _context.TransportUnits.AsQueryable();
            if(request.ArticleId!=null)
                qry = qry.Where(x => x.ArticleId == request.ArticleId);
            if (request.LocationId != null)
                qry = qry.Where(x => x.LocationId == request.LocationId);
            var res = await qry.ProjectTo<Model>()
                .OrderBy(x => x.Barcode)
                .ToListAsync(cancellationToken);
            return new Result(request)
            {
                TransportUnits = res
            };
        }
    }
}
