﻿using AutoMapper;
using WMS.Database.Models;

namespace WMS.API.Features.TransportUnits
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<TransportUnit, Index.Model>();
        }
    }
}
