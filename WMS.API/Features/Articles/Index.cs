﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WMS.Database;
using WMS.Database.Models;

namespace WMS.API.Features.Articles
{
    public class Index
    {
        public class Query : IRequest<Result>
        {

        }

        //public class QueryValidator : AbstractValidator

        public class Result
        {
            public List<Article> Articles { get; set; }
        }

        public class Handler : IRequestHandler<Query, Result>
        {
            private readonly WMSContext _context;

            public Handler(WMSContext context)
            {
                _context = context;
            }

            public async Task<Result> Handle(Query request, CancellationToken cancellationToken)
            {
                var articles = await _context.Articles.OrderBy(x => x.Code).ToListAsync(cancellationToken);
                return new Result
                {
                    Articles = articles
                };
            }
        }
    }
}