﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace WMS.BackendWorker
{
    public interface IKafkaClient
    {
        Task<Message<string, string>> Produce(string topic, string key, string value);
        Task<Message<string, string>> Produce(string topic, string key, object value);
        int Flush(int millisecondsTimeout);
    }

    public class KafkaClient : IKafkaClient
    {
        private readonly IConfiguration _globalConf;
        private readonly ILogger<KafkaClient> _logger;

        private Producer<string, string> _producer;
//        private static KafkaClient _instance;

        public KafkaClient(IConfiguration globalConf, ILogger<KafkaClient> logger)
        {
            _globalConf = globalConf;
            _logger = logger;

            string kafkaEndpoint = globalConf.GetValue<string>("Kafka:BootstrapServers");  //"127.0.0.1:29092";

            var config = new Dictionary<string, object>
            {
                { "bootstrap.servers", kafkaEndpoint },
                {"queued.min.messages", 1},
                {"socket.blocking.max.ms", "5"}, //min=1
            };

            _producer = new Producer<string, string>(config, new StringSerializer(Encoding.UTF8), new StringSerializer(Encoding.UTF8));
        }

        public async Task<Message<string, string>> Produce(string topic, string key, string value)
        {
            _logger.LogTrace("Sending message to topic '{0}' with key '{1}'", topic, key);
            return await _producer.ProduceAsync(topic, key, value);
        }

        public async Task<Message<string, string>> Produce(string topic, string key, object value)
        {
            _logger.LogTrace("Sending message to topic '{0}' with key '{1}'", topic, key);
            var ser = JsonConvert.SerializeObject(value, Formatting.None);
            return await _producer.ProduceAsync(topic, key, ser);
        }

        public int Flush(int millisecondsTimeout)
        {
            return _producer.Flush(millisecondsTimeout);
        }

//        public static KafkaClient Instance()
//        {
//            if(_instance == null)
//                _instance = new KafkaClient();
//            return _instance;
//        }
    }
}