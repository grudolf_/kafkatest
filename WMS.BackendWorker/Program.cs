﻿using System;
using System.IO;
using Microsoft.Extensions.DependencyInjection;

namespace WMS.BackendWorker
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();

            Startup startup = new Startup();
            startup.ConfigureServices(services);

            IServiceProvider serviceProvider = services.BuildServiceProvider();

            //run
            serviceProvider.GetService<Worker>().Run();
        }
    }
}
