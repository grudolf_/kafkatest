﻿using System;
using System.Collections.Generic;
using System.Text;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace WMS.BackendWorker
{
    /// <summary>
    /// Iz topica zaporedoma bere sporočila in jih obdeluje
    /// </summary>
    /// TODO: ErrorHandling? Sporočila, ki jih ne more obdelati bi mogoče morali premakniti v dead-letter? Zdaj crkne
    class Worker
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<Worker> _logger;
        private readonly Dispatcher _dispatcher;

        public Worker(IConfiguration configuration, ILogger<Worker> logger, Dispatcher dispatcher)
        {
            _configuration = configuration;
            _logger = logger;
            _dispatcher = dispatcher;
        }

        internal void Run()
        {
            Console.Title = "WMS.BackendWorker";

            string kafkaEndpoint = _configuration.GetValue<string>("Kafka:BootstrapServers");    // "127.0.0.1:29092";

            // The Kafka topic we'll be using
            string kafkaTopic = "wms.cmd";

            // Create the consumer configuration
            var consumerConfig = new Dictionary<string, object>
            {
                { "group.id", "wms.worker" },
                { "bootstrap.servers", kafkaEndpoint },
                //{ "queued.min.messages", 1 },
                { "socket.blocking.max.ms", "5" }, //min=1
                { "enable.auto.commit", false },
                { "statistics.interval.ms", 30000 },
                { "session.timeout.ms", 6000 },
//                { "auto.offset.reset", "smallest" },
            };

            using (var consumer = new Consumer<Ignore, string>(consumerConfig, null, new StringDeserializer(Encoding.UTF8)))
            {
                // Note: All event handlers are called on the main thread.

                consumer.OnPartitionEOF += (_, end)
                    => _logger.LogDebug($"Reached end of topic {end.Topic} partition {end.Partition}, next message will be at offset {end.Offset}");

                consumer.OnError += (_, error)
                    => _logger.LogError($"Error: {error}");

                // Raised on deserialization errors or when a consumed message has an error != NoError.
                consumer.OnConsumeError += (_, error)
                    => _logger.LogError($"Consume error: {error}");

                // Raised when the consumer is assigned a new set of partitions.
                consumer.OnPartitionsAssigned += (_, partitions) =>
                {
                    _logger.LogDebug($"Assigned partitions: [{string.Join(", ", partitions)}], member id: {consumer.MemberId}");
                    // If you don't add a handler to the OnPartitionsAssigned event,
                    // the below .Assign call happens automatically. If you do, you
                    // must call .Assign explicitly in order for the consumer to 
                    // start consuming messages.
                    consumer.Assign(partitions);
                };

                // Raised when the consumer's current assignment set has been revoked.
                consumer.OnPartitionsRevoked += (_, partitions) =>
                {
                    Console.WriteLine($"Revoked partitions: [{string.Join(", ", partitions)}]");
                    // If you don't add a handler to the OnPartitionsRevoked event,
                    // the below .Unassign call happens automatically. If you do, 
                    // you must call .Unassign explicitly in order for the consumer
                    // to stop consuming messages from it's previously assigned 
                    // partitions.
                    consumer.Unassign();
                };

                consumer.OnStatistics += (_, json)
                    => _logger.LogTrace($"Statistics: {json}");

                consumer.Subscribe(kafkaTopic);

                Console.WriteLine($"Started consumer, Ctrl-C to stop consuming");

                var cancelled = false;
                Console.CancelKeyPress += (_, e) => {
                    e.Cancel = true; // prevent the process from terminating.
                    cancelled = true;
                };

                while (!cancelled)
                {
                    if (!consumer.Consume(out Message<Ignore, string> msg, TimeSpan.FromMilliseconds(100)))
                    {
                        continue;
                    }

                    _logger.LogDebug($"Topic: {msg.Topic} Partition: {msg.Partition} Offset: {msg.Offset} {msg.Value}");
                    var message = new Msg.Message(msg.Value);
                    _dispatcher.Dispatch(message);

                    var committedOffsets = consumer.CommitAsync(msg).Result;
                    _logger.LogDebug($"Committed offset: {committedOffsets}");
                }
            }
        }
    }
}