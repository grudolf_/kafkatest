﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;
using WMS.BackendWorker.TransportUnits;
using WMS.Database;

namespace WMS.BackendWorker
{
    class Startup
    {
        public static IConfiguration Configuration { get; private set; }

        public Startup()
        {
            var builder = new ConfigurationBuilder()
                //.SetBasePath(Directory.GetCurrentDirectory())
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            Configuration = builder.Build();
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddDbContext<WMSContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("WMSContext")));

            //            services.AddSingleton<IMyConfiguration, MyConfiguration>();

            //            services.AddSingleton(new LoggerFactory()
            //                .AddNLog()
            //                .ConfigureNLog("nlog.config")
            //            );

            services.AddLogging(configure =>
            {
                configure.AddConfiguration(Configuration.GetSection("Logging"));
                configure.AddConsole();
            });
            services.AddSingleton<IKafkaClient, KafkaClient>();
            services.AddTransient<MoveCommandHandler>();
            services.AddTransient<Dispatcher>();
            services.AddTransient<Worker>();
        }
    }
}