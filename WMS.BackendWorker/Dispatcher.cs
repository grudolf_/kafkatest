﻿using System;
using System.Collections.Generic;
using WMS.Msg;
using WMS.Msg.TransportUnits;

namespace WMS.BackendWorker
{
    /// <summary>
    /// POC dispatch iz tipa sporočila na njegov handler. Neznana sporočila obdela splošni handler
    /// </summary>
    public class Dispatcher
    {
        private readonly IServiceProvider _provider;
        private static readonly IMessageHandler _defaultHandler = new DefaultMessageHandler();

        // Kaj obdelujemo, kdo obdeluje. Vsi morajo biti registrirani v DI. Ker imamo samo enega, to ni problem.
        private static Dictionary<Type, Type> _typeRegistry = new Dictionary<Type, Type>()
        {
            { typeof(MoveCommand), typeof(TransportUnits.MoveCommandHandler)}
        };

        public Dispatcher(IServiceProvider provider)
        {
            _provider = provider;
        }

        public void Dispatch(WMS.Msg.Message message)
        {
            //serviceProvider poskrbi za constructor
            var msgType = message.Body?.GetType();
            IMessageHandler handler = null;
            if (msgType != null && _typeRegistry.ContainsKey(msgType))
            {
                var handlerType = _typeRegistry[msgType];
                handler = _provider.GetService(handlerType) as IMessageHandler;
            }
            if (handler == null)
                handler = _defaultHandler;
            handler?.Handle(message);
        }
    }

    internal class DefaultMessageHandler : IMessageHandler
    {
        public void Handle(Message message)
        {
            Console.WriteLine($"Nothing to do for {message}");
        }
    }

    public interface IMessageHandler
    {
        void Handle(Message message);
    }

/*
    public interface IMessageHandler<T>
    {
        void Handle(Message<T> message);
    }
*/
}
