﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WMS.Database;
using WMS.Msg;
using WMS.Msg.TransportUnits;

namespace WMS.BackendWorker.TransportUnits
{
    internal class MoveCommandHandler : IMessageHandler
    {
        private WMSContext _ctx;
        private readonly IKafkaClient _kafkaClient;

        public MoveCommandHandler(WMSContext ctx, IKafkaClient kafkaClient)
        {
            _ctx = ctx;
            _kafkaClient = kafkaClient;
        }

        public void Handle(Message message)
        {
            var body = message.Body as WMS.Msg.TransportUnits.MoveCommand;
            Console.WriteLine($"Processing movement of {body.Id} from {body.SrcLocation} to {body.DstLocation}");
            var unit = _ctx.TransportUnits.FirstOrDefault(x => x.Id == body.Id);
            object outCmd = null;
            if (unit == null)
            {
                outCmd = new WMS.Msg.TransportUnits.InvalidUnitEvent()
                {
                    Id = body.Id,
                    SrcLocation = body.SrcLocation,
                    DstLocation = body.DstLocation,
                };
            }
            else
            {
                if (unit.LocationId != body.SrcLocation)
                {
                    outCmd = new WMS.Msg.TransportUnits.InvalidSrcLocationEvent()
                    {
                        Id = body.Id,
                        SrcLocation = body.SrcLocation,
                        ActualLocation = unit.LocationId,
                        DstLocation = body.DstLocation,
                    };
                }
                else
                {
                    unit.LocationId = body.DstLocation;
                    _ctx.SaveChanges();
                    outCmd = new WMS.Msg.TransportUnits.MovedEvent()
                    {
                        Id = body.Id,
                        SrcLocation = body.SrcLocation,
                        DstLocation = body.DstLocation,
                    };

                }
            }
            Console.WriteLine($"Processed movement of {body.Id} from {body.SrcLocation} to {body.DstLocation}");

            Dictionary<string, string> headers = new Dictionary<string, string>(){
                { Headers.Type, outCmd.GetType().FullName },
                { Headers.MessageId, Guid.NewGuid().ToString() },
                { Headers.CorrelationId, message.Headers[Headers.MessageId]},
                { Headers.DateCreated, DateTime.Now.ToString(CultureInfo.InvariantCulture) },
            };
            var msg = new Message(headers, outCmd);
            //var kafkaClient = KafkaClient.Instance();
            _kafkaClient.Produce("wms.event", body.Id.ToString(), msg).ConfigureAwait(false);
            _kafkaClient.Flush(50);
        }
    }
}