﻿using System;
using System.Collections.Generic;
using System.Text;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;

namespace QueLog
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Queue Logger";

            string kafkaEndpoint = "127.0.0.1:29092";

            // The Kafka topics we'll be using
            string[] kafkaTopic = { "wms.cmd", "wms.event" };

            // Create the consumer configuration
            var consumerConfig = new Dictionary<string, object>
            {
                { "group.id", "quelogger" },
                { "bootstrap.servers", kafkaEndpoint },
                //{ "queued.min.messages", 1 },
                {"socket.blocking.max.ms", "5"}, //min=1
            };

            // Create the consumer
            using (var consumer = new Consumer<Ignore, string>(consumerConfig, null, new StringDeserializer(Encoding.UTF8)))
            {
                // Subscribe to the OnMessage event
                consumer.OnMessage += (obj, msg) =>
                {
                    //Logging:
                    Console.WriteLine($"Received: {msg.Value}");
                };

                // Subscribe to the Kafka topic
                consumer.Subscribe(kafkaTopic);

                // Handle Cancel Keypress 
                var cancelled = false;
                Console.CancelKeyPress += (_, e) =>
                {
                    e.Cancel = true; // prevent the process from terminating.
                    cancelled = true;
                };

                Console.WriteLine("Ctrl-C to exit.");

                // Poll for messages
                while (!cancelled)
                {
                    consumer.Poll(500);
                }
            }
        }
    }
}
